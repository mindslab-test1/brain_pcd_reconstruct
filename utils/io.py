# -*- coding: utf-8 -*-
# @Author: Haozhe Xie
# @Date:   2019-08-02 10:22:03
# @Last Modified by:   Haozhe Xie
# @Last Modified time: 2020-02-22 19:13:01
# @Email:  cshzxie@gmail.com

import cv2
import h5py
import numpy as np
# import pyexr
# import open3d
import os
import sys
import pandas as pd
from pyntcloud import PyntCloud

from io import BytesIO

# References: http://confluence.sensetime.com/pages/viewpage.action?pageId=44650315

class IO:
    @classmethod
    def get(cls, file_path):
        _, file_extension = os.path.splitext(file_path)

        if file_extension in ['.png', '.jpg']:
            return cls._read_img(file_path)
        elif file_extension in ['.npy']:
            return cls._read_npy(file_path)
        elif file_extension in ['.h5']:
            return cls._read_h5(file_path)
        elif file_extension in ['.ply']:
            return cls._read_ply(file_path)
        elif file_extension in ['.txt']:
            return cls._read_txt(file_path)
        elif file_extension in ['.bin']:
            return cls._read_bin(file_path)
        elif file_extension in ['.obj']:
            return cls._read_obj(file_path)
        else:
            raise Exception('Unsupported file extension: %s' % file_extension)

    @classmethod
    def put(cls, file_path, file_content):
        _, file_extension = os.path.splitext(file_path)

        if file_extension in ['.h5']:
            return cls._write_h5(file_path, file_content)
        elif file_extension in ['.ply']:
            return cls._write_ply_xyz(file_path, file_content)
        elif file_extension in ['.bin']:
            return cls._write_bin(file_path, file_content)
        else:
            raise Exception('Unsupported file extension: %s' % file_extension)

    @classmethod
    def _read_img(cls, file_path):
        if mc_client is None:
            return cv2.imread(file_path, cv2.IMREAD_UNCHANGED) / 255.
        else:
            pyvector = mc.pyvector()
            mc_client.Get(file_path, pyvector)
            buf = mc.ConvertBuffer(pyvector)
            img_array = np.frombuffer(buf, np.uint8)
            img = cv2.imdecode(img_array, cv2.IMREAD_UNCHANGED)
            return img / 255.

    # References: https://github.com/numpy/numpy/blob/master/numpy/lib/format.py
    @classmethod
    def _read_npy(cls, file_path):
        if mc_client is None:
            return np.load(file_path)
        else:
            pyvector = mc.pyvector()
            mc_client.Get(file_path, pyvector)
            buf = mc.ConvertBuffer(pyvector)
            buf_bytes = buf.tobytes()
            if not buf_bytes[:6] == b'\x93NUMPY':
                raise Exception('Invalid npy file format.')

            header_size = int.from_bytes(buf_bytes[8:10], byteorder='little')
            header = eval(buf_bytes[10:header_size + 10])
            dtype = np.dtype(header['descr'])
            nd_array = np.frombuffer(buf[header_size + 10:], dtype).reshape(header['shape'])

            return nd_array

    @classmethod
    def _read_h5(cls, file_path):
        f = h5py.File(file_path, 'r')
        # Avoid overflow while gridding
        return f['data'][()] * 0.9

    # Read KITTI .bin point cloud
    @classmethod
    def _read_bin(cls, file_path):
        ptcloud = np.fromfile(str(file_path), dtype=np.float32, count=-1).reshape([-1, 4])
        return ptcloud

    @classmethod
    def _read_ply(cls, file_path):
        cloud = PyntCloud.from_file(file_path)
        return cloud.points.to_numpy()

    @classmethod
    def _read_obj(cls, file_path):
        def parse_vertex(vstr):
            # parses a vertex record as either vid, vid/tid, vid//nid or vid/tid/nid
            # and returns a 3-tuple where unparsed values are replaced with -1
            vals = vstr.split('/')
            vid = int(vals[0]) - 1
            tid = int(vals[1]) - 1 if len(vals) > 1 and vals[1] else -1
            nid = int(vals[2]) - 1 if len(vals) > 2 else -1
            return [vid, tid, nid]

        with open(file_path, 'r') as obj:
            data = obj.read()

            lines = data.splitlines()
            vertices = []
            face_indices = []

            for line in lines:
                element = line.split()
                if element:
                    if element[0] == 'v':
                        v = [float(element[1]), float(element[2]), float(element[3])]
                        vertices.append(v)
                    elif element[0] == 'f':
                        face = [parse_vertex(vstr)[0] for vstr in element[1:]]
                        face_indices.append(face)
            return np.array(vertices), np.array(face_indices).astype(int)

    @classmethod
    def _read_txt(cls, file_path):
        return np.loadtxt(file_path)

    @classmethod
    def _write_h5(cls, file_path, file_content):
        with h5py.File(file_path, 'w') as f:
            f.create_dataset('data', data=file_content)

    @classmethod
    def _write_ply_xyz(cls, file_path, file_content):
        points = pd.DataFrame(file_content[:, :3], columns=['x', 'y', 'z'])
        cloud_new = PyntCloud(points)
        cloud_new.to_file(file_path)

    @classmethod
    def _write_bin(cls, file_path, file_content):
        file_content.tofile(file_path)
