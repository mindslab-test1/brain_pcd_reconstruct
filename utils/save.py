import torch


# convenient model save/resume for encoder pretraining
def save_pretrain(model, optimizer, epoch,  path):
    d = {
        'epoch': epoch,
        'model_state_dict': model.state_dict(),
        'optimizer_state_dict': optimizer.state_dict(),
    }
    torch.save(d, path)


def resume_pretrain(path, model, optimizer):
    checkpoint = torch.load(path, map_location='cpu')
    try:
        model.load_state_dict(checkpoint['model_state_dict'], strict=True)
    except RuntimeError:
        print("INFO: this model is trained with DataParallel. Creating new state_dict without module...")
        state_dict = checkpoint["model_state_dict"]

        from collections import OrderedDict
        new_state_dict = OrderedDict()

        for k, v in state_dict.items():
            name = k[7:]  # remove `module.`
            new_state_dict[name] = v
        model.load_state_dict(new_state_dict)

    optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    start_epoch = checkpoint['epoch']

    return model, optimizer, start_epoch


def save(model, optimizer, epoch, scheduler, valid_loss, log_dir, path):
    d = {
        'epoch': epoch,
        'model': model.state_dict(),
        'optimizer': optimizer.state_dict(),
        'scheduler': scheduler.state_dict(),
        'valid_loss': valid_loss,
        'log_dir': log_dir
    }
    torch.save(d, path)


def resume(path, model, optimizer, scheduler):
    ckpt = torch.load(path, map_location='cpu')
    try:
        model.load_state_dict(ckpt['model'], strict=True)
    except RuntimeError:
        print("INFO: this model is trained with DataParallel. Creating new state_dict without module...")
        state_dict = ckpt["model"]
        from collections import OrderedDict
        new_state_dict = OrderedDict()
        for k, v in state_dict.items():
            name = k[7:]  # remove `module.`
            new_state_dict[name] = v
        model.load_state_dict(new_state_dict)
    optimizer.load_state_dict(ckpt['optimizer'])
    scheduler.load_state_dict(ckpt['scheduler'])
    start_epoch = ckpt['epoch']
    valid_loss = ckpt['valid_loss']
    log_dir = ckpt['log_dir']

    return model, optimizer, scheduler, start_epoch, valid_loss, log_dir
