import torch
import numpy as np


class RandomPointSampling(torch.nn.Module):
    def __init__(self, n_points):
        super(RandomPointSampling, self).__init__()
        self.n_points = n_points

    def forward(self, cloud_org):
        _ptcloud = torch.split(cloud_org, 1, dim=0)
        ptclouds = []
        for p in _ptcloud:
            non_zeros = torch.sum(p, dim=2).ne(0)
            p = p[non_zeros].unsqueeze(dim=0)
            n_pts = p.size(1)
            if n_pts < self.n_points:
                rnd_idx = torch.cat([torch.randint(0, n_pts, (self.n_points, ))])
            else:
                rnd_idx = torch.randperm(p.size(1))[:self.n_points]
            ptclouds.append(p[:, rnd_idx, :])

        return torch.cat(ptclouds, dim=0).contiguous()


class MeshSampling(object):
    def __init__(self, n_pts):
        self.n_pts = n_pts

    @staticmethod
    def triangle_area(v1, v2, v3):
        return .5 * np.linalg.norm(np.cross(v2 - v1, v3 - v1), axis=1)

    def weighted_sample(self, vertices, face_idxes):
        """
        :param n_pts: number of points to sample
        :param vertices: Coord array of vertices. np.array[v,3], float
        :param face_idxes: Face index array. np.array[f,3], int
        :return: sampled points. np.array[n_pts, 3]
        """
        face_idxes = face_idxes.astype(int)

        # Weighted selection by face areas
        v1_faces = vertices[face_idxes[:, 0]]
        v2_faces = vertices[face_idxes[:, 1]]
        v3_faces = vertices[face_idxes[:, 2]]
        areas = self.triangle_area(v1_faces, v2_faces, v3_faces)
        probabilities = areas / areas.sum()
        weighted_random_indices = np.random.choice(range(len(areas)), size=self.n_pts, p=probabilities)

        # Barycentric sampling on faces
        v1_xyz = v1_faces[weighted_random_indices]
        v2_xyz = v2_faces[weighted_random_indices]
        v3_xyz = v3_faces[weighted_random_indices]

        u = np.random.rand(self.n_pts, 1)
        v = np.random.rand(self.n_pts, 1)
        out_triangle = (u + v > 1)
        u[out_triangle] = 1. - u[out_triangle]
        v[out_triangle] = 1. - v[out_triangle]
        w = 1. - (u + v)

        result_xyz = (v1_xyz * u) + (v2_xyz * v) + (v3_xyz * w)
        result_xyz = result_xyz.astype(np.float32)
        return result_xyz




