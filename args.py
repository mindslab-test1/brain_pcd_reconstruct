import argparse
import os


def get_parser():
    # command line args
    parser = argparse.ArgumentParser(description='TensorFlow Experiment')
    parser = add_args(parser)
    return parser


def get_args():
    parser = get_parser()
    args = parser.parse_args()
    # if args.save_dir is None:
    #     args.save_dir = os.path.join("results", args.cates[0], "TensorFlow")

    return args


def add_args(parser):
    # Running options
    parser.add_argument('--device', type=int, default=1,
                        help='GPU id to use.')
    parser.add_argument('--resume_checkpoint', type=str, default=None,
                        help='Path to the checkpoint to be loaded for training.')
    parser.add_argument('--load_checkpoint', type=str, default="./checkpoints/epoch_30_encoder.pth",
                        help='Path to the checkpoint to be loaded for generation and test.')
    parser.add_argument('--save_dir', type=str, default=None)
    parser.add_argument('--seed', type=int, default=1234,
                        help='Seed for initializing training. ')

    # Dataset
    parser.add_argument('--dataset', type=str, default='ShapeNet',
                        help='Using dataset')
    parser.add_argument('--subset', type=int, default=0,
                        help='dataset subset. 0:train, 1:val, 2:test')
    parser.add_argument('--dataset_dir', type=str, default='/DATA2/ShapeNet/ShapeNetCore.v2',
                        help='dataset directory')
    parser.add_argument('--n_pts_original', type=int, default=2048,
                        help='#sampled points from dataset')

    # Logging and saving frequency
    parser.add_argument('--viz_freq',   type=int, default=400)
    parser.add_argument('--log_freq',   type=int, default=1)
    parser.add_argument('--save_freq',  type=int, default=100)
    parser.add_argument('--valid_freq', type=int, default=100)

    # Training setting
    parser.add_argument('--batch_size',     type=int,   default=80,     help='batch size (of datasets) for training')
    parser.add_argument('--freeze_encoder', type=bool,  default=True,   help='whether to freeze encoder.')
    parser.add_argument('--lr',             type=float, default=5e-3,   help='learning rate for the Adam optimizer.')
    parser.add_argument('--beta1',          type=float, default=0.9,    help='beta1 for Adam.')
    parser.add_argument('--beta2',          type=float, default=0.999,  help='beta2 for Adam.')
    parser.add_argument('--weight_decay',   type=float, default=2,      help='weight decay for the optimizer.')
    parser.add_argument('--step_size',      type=int,   default=5000,   help='step size for scheduler.')
    parser.add_argument('--gamma',          type=int,   default=0.25,   help='learning rate decay ratio.')
    parser.add_argument('--stop_scheduler', type=int,   default=15000,  help='when to freeze learning rate.')

    # Latent variable setting
    parser.add_argument('--max_deg',        type=int,   default=2,      help='max representation degree of z.')
    parser.add_argument('--channel_z',      type=int,   default=128,    help='#channels of z.')

    # Preprocessing
    parser.add_argument('--n_pts',          type=int,   default=128,    help='#pts for encoder input sampling.')
    parser.add_argument('--n_nbhd',         type=int,   default=8,      help='#nbhds for KNN graph.')
    parser.add_argument('--mask_rate',      type=float, default=0.15,   help='masked point rate for pretraining.')
    parser.add_argument('--random_rate',    type=float, default=0.5,    help='random point rate among masked points.')
    parser.add_argument('--std_min',        type=float, default=0.0,    help='min point noise size')
    parser.add_argument('--std_max',        type=float, default=0.075,  help='max point noise size')
    parser.add_argument('--test_std',       type=float, default=0.0,    help='point noise for sampling')
    parser.add_argument('--test_std_y',     type=float, default=1.0,    help='std for gaussian point sampling')
    parser.add_argument('--std_scale',      type=float, default=2.0,    help='point noise scale in AR layer input')

    # Encoder Architecture
    parser.add_argument('--enc_nlayers',    type=int,   default=5,      help='#layers in SE(3)-transformer.')
    parser.add_argument('--enc_hidden',     type=int,   default=8,      help='#hidden channels in transformer.')
    parser.add_argument('--enc_n_heads',    type=int,   default=2,      help='#heads for transformer.')

    # Decoder Architecture
    parser.add_argument('--dec_num_flows',  type=int,   default=8,      help='#layers in SoftPointFlow.')
    parser.add_argument('--dec_hidden',     type=int,   default=256,    help='#hidden channels in SoftPointFlow.')

    # Loss weights
    parser.add_argument('--var_weight',     type=float, default=0.1,    help='weight for variance regularization.')
    parser.add_argument('--prior_weight',   type=float, default=10.0,   help='weight for prior penalty.')

    return parser
