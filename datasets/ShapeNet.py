import torch
import os
import json
from utils.io import IO
from utils.nsample import RandomPointSampling, MeshSampling
from args import get_args

class ShapeNetDataset(torch.utils.data.dataset.Dataset):
    def __init__(self, args):
        self.n_pts = args.n_pts_original
        self.subset = args.subset
        self.file_list = self._get_file_list()
        self.dataset_dir = args.dataset_dir

    def __len__(self):
        return len(self.file_list)

    def __getitem__(self, idx):
        obj_dir = self.file_list[idx]
        vertices, face_indices = IO.get(obj_dir)

        # weighted barycentric sampling from mesh
        mesh_sampled = MeshSampling(self.n_pts).weighted_sample(vertices, face_indices)
        ptcloud_raw = torch.tensor(mesh_sampled)  # [n_pts, 3]

        # normalize to N(0,1)
        ptcloud_avg = ptcloud_raw.mean(dim=0, keepdim=True)  # [1, 3]
        ptcloud_std = ptcloud_raw.std(dim=0, keepdim=True)  # [1, 3]
        ptcloud_org = (ptcloud_raw - ptcloud_avg) / (ptcloud_std.norm(dim=-1, keepdim=True) + 1e-12)

        # return: torch.size([n_pts, 3])
        return ptcloud_org

    def _get_file_list(self):
        with open('./datasets/ShapeNet.json', 'r') as f:
            file_list = json.load(f)
        dataset_list = []
        label = ['train', 'val', 'test']
        for fdict in file_list:
            dataset_list += fdict[label[self.subset]]
        return dataset_list


def _make_file_list(dataset_dir):
    """Prepare file list for the dataset"""
    file_list = list()

    idx = 0
    taxonomy_ids = os.listdir(dataset_dir)
    for taxonomy_id in taxonomy_ids:
        taxonomy_dir = os.path.join(dataset_dir, taxonomy_id)
        ext = os.path.splitext(taxonomy_dir)[-1]
        if ext != '.json':
            obj_dirs = os.listdir(taxonomy_dir)
            obj_dict = dict()
            obj_dict["train"] = []
            obj_dict["val"] = []
            obj_dict["test"] = []
            for obj_dir in obj_dirs:
                obj_model_dir = os.path.join(taxonomy_dir, obj_dir, 'models', 'model_normalized.obj')
                if idx % 30 < 25:  # train
                    if os.path.exists(obj_model_dir):
                        obj_dict["train"].append(obj_model_dir)
                elif idx % 30 > 26:  # val
                    if os.path.exists(obj_model_dir):
                        obj_dict["val"].append(obj_model_dir)
                elif idx % 30 == 25 or idx % 30 == 26:  # test
                    if os.path.exists(obj_model_dir):
                        obj_dict["test"].append(obj_model_dir)
                idx += 1
                print(idx, obj_dir)
            file_list.append(obj_dict)
    with open('ShapeNet.json', 'w') as f:
        json.dump(file_list, f, indent='\t')
    return file_list


if __name__ == '__main__':
    args = get_args()
    _make_file_list(args.dataset_dir)
