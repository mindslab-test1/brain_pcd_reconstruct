from easydict import EasyDict as edict
import torch

__C = edict()
cfg = __C

__C.device = torch.device('cuda:1')
__C.subset = 0  # train

# Training options
__C.train = edict()
__C.train.batch_size = 80
__C.train.n_original = 2048
__C.train.n_sampling_Disc = 256
__C.train.n_sampling_Q = 128
__C.train.n_generate = 256
__C.train.n_epochs = 10000
__C.train.lr = 0.001


__C.models = edict()
"""
Q model design
n_nbhd: knn graph degree
num_layers: #Gblocks (GSE3Res + GNorm)
num_normlayers: #FCblocks (GNorm)
mid_channels: #channel of hidden layers
out_channels: #channel of out psi's
div: Inside GSE3Res, out #channel of GConvSE3Partial is divided by div
n_heads: # of attention heads
"""
__C.models.inference = edict()
__C.models.inference.n_nbhd = 8
__C.models.inference.num_layers = 5
__C.models.inference.num_fclayers = 1
__C.models.inference.mid_channels = 8
__C.models.inference.out_channels = 128
__C.models.inference.num_degrees = 3
__C.models.inference.div = 2
__C.models.inference.n_heads = 2
"""
G model design
dim_noise: dimension of z
dim_psi0, dim_psi1: #channel of psi's, same with encoder.out_channels
channel_scalar, channel_vector: #channel of hidden layers
n_layers: #layers
"""
__C.models.generator = edict()
__C.models.generator.dim_noise = 16
__C.models.generator.dim_psi = 256
__C.models.generator.n_channel = 256
__C.models.generator.n_layers = 5
"""
D model design
"""
__C.models.discriminator = edict()
__C.models.discriminator.hidden_layer = 256
__C.models.discriminator.lr = 0.01
__C.models.discriminator.iters = 30
"""
Dataset
"""
__C.datasets = edict()
"""
ShapeNet
"""
__C.datasets.ShapeNet = edict()
__C.datasets.ShapeNet.dir = '/DATA2/ShapeNet/ShapeNetCore.v2'


