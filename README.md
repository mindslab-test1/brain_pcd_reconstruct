# pcd_comp1
SE(3)-equivariant Transformer + masked language modelling 기반 sparse-to-dense point cloud completion 엔진

## Engine information
* **Engine name:** pcd_comp1

## Software / hardware requirements
* **OS:** Ubuntu18.04
* **Python version:** MUST be >= 3.6
* **Pytorch / Tensorflow version:**  PyTorch 1.7.0
* **minimum GPU spec:** V100 / DRAM >= 6GB
* V100 30번 서버 docker image Completion1:0.1 환경

## Model file:
* **Class path:** model/encoder.py, model/preprocess.py
* **Checkpoint / saved model file path:** checkpoints/epoch_30_encoder.pth

## Install
1. `pip install -r requirement.txt`

## Docker run
Port: `17107`

Image build: 
* `cd {project_root}`
* `docker build -t completion:1.0 .`

Container 실행: 
* `docker run --name pcd_comp1_server --ipc=host --gpus 'device={GPU_IDS}' --rm -dit completion:1.0 /bin/bash` 
* (GPU_IDS에 할당 GPU 입력. 예시: 'device=1')

client.py 테스트:
* `docker exec -it pcd_comp1_server /bin/bash`
* `/app/venv_completion1/bin/python3 client.py --port 17107 --input {input object file} --output {output object file}`
* 가능한 file 확장자: .ply, .bin
* default값: ./samples/sample_chair.ply -> ./samples/out_chair.ply
