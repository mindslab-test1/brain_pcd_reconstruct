import time
import grpc
from concurrent import futures
import os

import torch
from args import get_parser
from model.preprocess import MakeGraph
from model.encoder import PointEncoder_SE3
from utils.save import resume_pretrain
from utils.nsample import RandomPointSampling

from completion1_pb2 import Completion1_output
from completion1_pb2_grpc import Completion1Servicer, add_Completion1Servicer_to_server


class Completion1Server(Completion1Servicer):
    def __init__(self, args):
        # make class
        self.device = args.device

        self.graph_maker = MakeGraph(
            args.n_nbhd,
            device=self.device,
            use_cls=True,
            mask_rate=args.mask_rate,
            random_rate=args.random_rate).eval()

        self.encoder = PointEncoder_SE3(
            num_layers=args.enc_nlayers,
            hidden=args.enc_hidden,
            num_degrees=args.max_deg + 1,
            attn_heads=args.enc_n_heads
        )

        self.encoder_sample = RandomPointSampling(128)

        optimizer = torch.optim.Adam(self.encoder.parameters(), lr=args.lr)
        model_path = args.load_checkpoint
        self.encoder, optimizer, start_epoch = resume_pretrain(model_path, self.encoder, optimizer)

        self.encoder = self.encoder.to(self.device).eval()

    def Completion(self, request, context):
        req_data = request.data
        ptcloud_raw = torch.tensor(req_data).view(-1, 3)  # [n_pts, 3]

        # normalize to N(0,1)
        ptcloud_avg = ptcloud_raw.mean(dim=0, keepdim=True)  # [1, 3]
        ptcloud_std = ptcloud_raw.std(dim=0, keepdim=True)  # [1, 3]
        ptcloud_org = (ptcloud_raw - ptcloud_avg) / (ptcloud_std.norm(dim=-1, keepdim=True) + 1e-12)  # [n_pts, 3]

        # Randomsample and make batch graph

        random_rate_max = 0.3
        perturb_rate_max = 0.09
        rates = [[i/4. * random_rate_max, (3 - i)/4. * perturb_rate_max] for i in range(4)]
        outs = []
        for i in range(4):
            inp_ptcloud = ptcloud_org.unsqueeze(0).expand(4, -1, -1)  # [4, n_pts, 3]
            inp_ptcloud = self.encoder_sample(inp_ptcloud).to(self.device)  # [16, 128, 3]
            inp_ptcloud = inp_ptcloud + torch.randn_like(inp_ptcloud) * rates[i][1]
            self.graph_maker.random_rate = rates[i][0]
            G_batch = self.graph_maker(inp_ptcloud)
            cls_nodes = G_batch.ndata.pop('cls')

            # Run encoder
            _, out = self.encoder(G_batch)  # [n_total_nodes, 1, 3]
            outs.append(out[~cls_nodes])
        ptcloud_out = torch.cat(outs, dim=0).view(-1, 3)  # [16 * 128, 3]
        ptcloud_out = ptcloud_out * ptcloud_std.to(self.device) + ptcloud_avg.to(self.device)
        out_data = ptcloud_out.view(-1).detach().cpu().numpy().tolist()

        # Do sth
        return Completion1_output(data=out_data)


if __name__ == '__main__':

    parser = get_parser()
    parser.add_argument('--port', type=int, default=17107,
                        help='gRPC server port.')
    args = parser.parse_args()

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    completion_server = Completion1Server(args=args)
    completion_servicer = add_Completion1Servicer_to_server(completion_server, server)
    server.add_insecure_port("[::]:{}".format(args.port))
    server.start()

    try:
        while True:
            time.sleep(60 * 60 * 24)
    except KeyboardInterrupt:
        server.stop(0)
