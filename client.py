import grpc

from args import get_parser
from utils.io import IO
import numpy as np
import torch

from completion1_pb2 import Completion1_input
from completion1_pb2_grpc import Completion1Stub


class Completion1Client:
    def __init__(self, ip="localhost", port=17107):
        self.server_ip = ip
        self.server_port = port

        self.stub = Completion1Stub(
            grpc.insecure_channel(self.server_ip + ":" + str(self.server_port))
        )

    def get_completion(self, ptcloud_in):
        myinput = Completion1_input()
        myinput.data.extend(torch.tensor(ptcloud_in).reshape(-1).numpy().tolist())

        myservice_out = self.stub.Completion(myinput)
        ptcloud_out = torch.tensor(myservice_out.data).reshape(-1, 3).numpy()

        return ptcloud_out


if __name__ == '__main__':
    parser = get_parser()
    parser.add_argument('--port', type=int, default=17107,
                        help='gRPC server port.')
    parser.add_argument("--ip", type=str, default='localhost',
                        help="IP address")
    parser.add_argument("--input", type=str, default='./samples/sample_chair.ply',
                        help="path for input point cloud")
    parser.add_argument("--output", type=str, default='./samples/out_chair.ply',
                        help="path for output point cloud")
    args = parser.parse_args()

    completion_client = Completion1Client(ip=args.ip, port=args.port)

    ptcloud_np = IO.get(args.input)
    ptcloud_output = completion_client.get_completion(ptcloud_np)
    IO.put(args.output, ptcloud_output)
