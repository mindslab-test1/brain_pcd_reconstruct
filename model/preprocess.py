import torch
import torch.nn as nn
from sklearn.neighbors import kneighbors_graph as knng
import dgl
from dgl import graph, batch, function
import math


class MakeGraph(nn.Module):
    def __init__(self, n_nbhd, device, use_cls=True, mask_rate=0.5, random_rate=0.7):
        super(MakeGraph, self).__init__()
        self.n_nbhd = n_nbhd
        self.device = device
        self.use_cls = use_cls

        self.mask_rate = mask_rate
        self.random_rate = random_rate
        self.eps = 1e-12

    def make_single_graph(self, ptcloud):
        n_pts = ptcloud.size(0)
        KNNG = knng(ptcloud.cpu().numpy(), self.n_nbhd, mode='connectivity', include_self=False).tocoo()
        src = torch.tensor(KNNG.row, dtype=torch.long)
        dst = torch.tensor(KNNG.col, dtype=torch.long)

        # Add cls token vertex
        if self.use_cls:
            src = torch.cat([src, torch.ones([n_pts], dtype=torch.long) * n_pts, torch.arange(n_pts)], dim=0)
            dst = torch.cat([dst, torch.arange(n_pts), torch.ones([n_pts], dtype=torch.long) * n_pts], dim=0)

        src = src.to(self.device)
        dst = dst.to(self.device)

        # Assign node data
        num_nodes = n_pts
        node_data = ptcloud.unsqueeze(1)  # [n_pts, 1, 3]

        # Randomly mask nodes
        masked_node_data, masked_nodes = self.make_random_mask(node_data)

        # cls token vertex is located at origin
        if self.use_cls:
            num_nodes += 1
            node_data = torch.cat([node_data, torch.tensor([[[0., 0., 0.]]]).to(self.device)], dim=0)
            masked_node_data = torch.cat([masked_node_data, torch.tensor([[[0., 0., 0.]]]).to(self.device)], dim=0)
            masked_nodes = torch.cat([masked_nodes, torch.tensor([[False]]).to(self.device)], dim=0)

        # Make graph
        G = graph((src, dst), num_nodes=num_nodes)
        G.ndata['x'] = masked_node_data     # [n_nodes, 1, 3]
        G.ndata['x_original'] = node_data   # [n_nodes, 1, 3]
        G.ndata['mask'] = masked_nodes      # [n_nodes, 1]
        G.edata['d'] = G.ndata['x'][dst] - G.ndata['x'][src]  # [n_edges, 1, 3]
        if self.use_cls:
            cls_node = torch.cat([torch.full([n_pts], fill_value=False, dtype=torch.bool),
                                  torch.tensor([True])], dim=0).to(self.device)
            G.ndata['cls'] = cls_node  # [n_nodes]
        G.ndata['prob'] = self.pdf_estimation(G, src, dst)  # [n_nodes, 1]
        G = G.to(self.device)
        return G

    def make_random_mask(self, node_data):
        """
        Randomly replace some node coordinates with randn(0,1), while half of mask is not replaced
        :param node_data: [n_pts, 1, 3]
        :return:
        node_data: masked data [n_pts, 1, 3]
        masked_nodes: boolean tensor [n_pts, 1], indicates masked nodes
        """
        n_pts = node_data.size(0)

        randomness = torch.rand([n_pts, 1], device=self.device)
        masked_nodes = (randomness < self.mask_rate)
        random_mask_nodes = (randomness < self.mask_rate * self.random_rate)

        # half
        masked_node_data = node_data.clone().detach()
        masked_node_data[random_mask_nodes] = torch.randn_like(node_data)[random_mask_nodes]
        return masked_node_data, masked_nodes

    def pdf_estimation(self, G, src, dst):
        with G.local_scope():
            # calculate d^3
            d_original = G.ndata['x_original'][dst] - G.ndata['x_original'][src]  # [n_edges, 1, 3]
            G.edata['d_cube'] = torch.pow((d_original * d_original).sum(dim=-1, keepdim=False), exponent=1.5)
            if self.use_cls:
                G.edata['d_cube'][G.ndata['cls'][dst]] = 0  # [n_edges, 1]

            # calculate local sphere volume
            rG = dgl.reverse(G, copy_ndata=True, copy_edata=True)
            rG.update_all(function.copy_e('d_cube', 'm'), function.max('m', 'max_dist_cube'))
            node_local_volume = rG.ndata['max_dist_cube'] * 4 * math.pi / 3.

        # calculate probs
        prob_node = (1. + self.n_nbhd) / (node_local_volume + self.eps)  # [n_nodes, 1]
        if self.use_cls:
            prob_node[G.ndata['cls']] = 0
        return prob_node

    def gaussian_blur_convolution(self, G, perturbs, perturbs_std):
        """
        Calcuates
        :param G: Batch graph containing 'x_original', 'prob', 'cls'
        :param perturbs: point perturbation, Tensor size [n_nodes, 1, 3]
        :param perturbs_std: point perturbation std, Tensor size [n_nodes, 1, 1]
        :return: prob_blur, [n_nodes, 1, 1]
        """
        rG = dgl.add_self_loop(dgl.reverse(G, copy_ndata=False, copy_edata=False))
        with rG.local_scope():
            rG.ndata['x_original'] = G.ndata['x_original']              # [n_nodes, 1, 3]
            rG.ndata['x_perturbed'] = G.ndata['x_original'] + perturbs  # [n_nodes, 1, 3]
            rG.ndata['prob_org'] = G.ndata['prob'].unsqueeze(-1)        # [n_nodes, 1, 1]
            rG.ndata['perturbs_std'] = perturbs_std                     # [n_nodes, 1, 1]

            # gausian blur propagation weight
            rG.apply_edges(function.copy_u('perturbs_std', 'stds'))
            rG.apply_edges(function.u_sub_v('x_original', 'x_perturbed', 'dist_vec'))
            rG.edata['weight'] = 1. / (self.eps + math.sqrt(2. * math.pi) * rG.edata['stds']) * torch.exp(
                (rG.edata['dist_vec'] * rG.edata['dist_vec']).sum(dim=-1, keepdim=True) / (-2. * perturbs_std ** 2))
            rG.update_all(function.copy_e('weight', 'msg'), function.sum('msg', 'total_weight'))
            # stds:         [n_edges, 1, 1]
            # dist_vec:     [n_edges, 1, 3]
            # weight:       [n_edges, 1, 1]
            # total_weight: [n_nodes, 1, 1]

            # prob propagation
            rG.update_all(function.u_mul_e('prob_org', 'weight', 'msg'), function.sum('msg', 'prob_sum'))
            prob_blur = rG.ndata['prob_sum'] / rG.ndata['total_weight']
            # prob_sum:     [n_nodes, 1, 1]
            # prob_blur:    [n_nodes, 1, 1]

        if self.use_cls:
            # eliminate cls prob
            prob_blur[G.ndata['cls']] = 0.

        return prob_blur

    def gaussian_blur_entropy_loss(self, G, perturbs, perturbs_std):
        """
        :param G: Batch graph containing 'x_original', 'prob', 'cls'
        :param perturbs: point perturbation, Tensor size [n_nodes, 1, 3]
        :param perturbs_std: point perturbation std, Tensor size [n_nodes, 1, 1]
        :return: Tensor size [batch_size, 1]
        """
        # return sum E[log p]
        with G.local_scope():
            G.ndata['prob_blur'] = self.gaussian_blur_convolution(G, perturbs, perturbs_std).squeeze(-1)
            # [n_nodes, 1]
            return dgl.readout_nodes(G, 'prob_blur', op='mean')

    def forward(self, ptclouds):
        # ptclouds: batch of randomsampled pointclouds, size: [batch_size, n_pts, 3]
        # Make graph from ptcloud
        Gs = []
        for ptcloud in ptclouds:
            Gs.append(self.make_single_graph(ptcloud))
        # Make batch graph
        G_batch = batch(Gs)
        return G_batch
