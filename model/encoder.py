import torch

from model.se3.modules import GConvSE3, GNormSE3, get_basis_and_r, GSE3Res
from model.se3.fibers import Fiber


class PointEncoder_SE3(torch.nn.Module):
    """SE(3) equivariant GCN with attention"""
    def __init__(self,
                 num_layers,
                 hidden,
                 num_degrees,
                 attn_heads):
        """
        :param num_layers: # transformer layers
        :param hidden: # hidden channels
        :param num_degrees: # type of SO(3)-representation
        :param attn_heads: # attention heads
        """
        super(PointEncoder_SE3, self).__init__()
        # Build the network
        self.num_layers = num_layers
        self.hidden = hidden
        self.num_degrees = num_degrees
        self.div = 1
        self.attn_heads = attn_heads

        self.fibers = {'in': Fiber(structure=[(1, 1)]),
                       'mid': Fiber(self.num_degrees, self.hidden),
                       'out': Fiber(structure=[(1, 1)])}

        blocks = self._build_gcn(self.fibers)
        self.Gblock = blocks
        self.GConv = GConvSE3(self.fibers['mid'], self.fibers['out'], self_interaction=True)

    def _build_gcn(self, fibers):
        # Equivariant layers
        Gblock = []
        fin = fibers['in']
        for i in range(self.num_layers):
            Gblock.append(GSE3Res(fin, fibers['mid'], div=self.div, n_heads=self.attn_heads))
            Gblock.append(GNormSE3(fibers['mid'], nonlin=torch.nn.ReLU(inplace=True), num_layers=0))
            fin = fibers['mid']

        return torch.nn.ModuleList(Gblock)

    def forward(self, G):
        # G: Batch graph
        # Compute equivariant weight basis from relative positions
        basis, r = get_basis_and_r(G, self.num_degrees-1)

        # encoder (equivariant layers)
        h = {'1': G.ndata['x']}
        for layer in self.Gblock:
            h = layer(h, G=G, r=r, basis=basis)

        last_out = {}
        for k, v in h.items():
            last_out[k] = v.clone()  # {'d': [N, hidden, 2d+1] for d in range(num_degrees)}

        out = self.GConv(h, G=G, r=r, basis=basis)['1']  # [N, 1, 3]
        return last_out, out


if __name__ == '__main__':
    from utils.parameter_inspection import get_n_params
    model = PointEncoder_SE3(num_layers=6, hidden=12, num_degrees=3, attn_heads=3)
    print(get_n_params(model))

