FROM nvcr.io/nvidia/cuda:10.2-runtime-ubuntu18.04

WORKDIR /app

RUN set -xe \
 && apt-get -y update \
 && apt-get install -y python3-pip python3-dev
RUN pip3 install --upgrade pip
RUN apt-get -y update \
 && apt-get install -y git vim libsm6 libxext6 libxrender-dev libgl1-mesa-glx

ENV PYTHONIOENCODING UTF-8

COPY requirements.txt ./

RUN apt-get install -y python3-venv \
 && python3 -m venv venv_completion1 \
 && venv_completion1/bin/pip3 install --upgrade pip setuptools \
 && venv_completion1/bin/pip3 install -r ./requirements.txt

COPY . ./PCGAN
WORKDIR PCGAN

EXPOSE 17107
ENTRYPOINT /app/venv_completion1/bin/python3 server.py --load_checkpoint ./checkpoints/epoch_30_encoder.pth --device 0 --port 17107
