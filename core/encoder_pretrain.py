import torch
from torch import optim
from torch.utils.data import DataLoader



from model.encoder import PointEncoder_SE3
from model.preprocess import MakeGraph
from datasets.ShapeNet import ShapeNetDataset
from utils.nsample import RandomPointSampling

from utils.save import resume_pretrain
from torch.utils.tensorboard import SummaryWriter

from args import get_args
import os

train_writer = SummaryWriter()


def pretrain_epoch(epoch, graph_maker, encoder, dataloader, optimizer, args):
    encoder.train()

    encoder_sample = RandomPointSampling(args.n_pts)     #################################
    batch_size = args.batch_size
    iter_len = len(dataloader)

    for it, ptclouds in enumerate(dataloader):
        with torch.no_grad():
            ptclouds = ptclouds.to(args.device)
        # ptcloud: [batch_size, 2048, 3]
        # Each are normalized to N(0,1)

        # Make encoder input
        sampled_clouds = []
        for ptcloud in ptclouds:
            sampled_clouds.append(encoder_sample(ptcloud.unsqueeze(0)))
        inp_ptcloud = torch.cat(sampled_clouds, 0)
        # print(inp_ptcloud.shape)  # [batch_size, 128, 3]

        # Make graph batch
        G_batch = graph_maker(inp_ptcloud)
        x_original = G_batch.ndata.pop('x_original')
        # print(x_original.shape)  # [N, 1, 3]
        masked_nodes = G_batch.ndata.pop('mask')
        # print(masked_nodes.shape)  # [N, 1]
        cls_nodes = G_batch.ndata.pop('cls')

        optimizer.zero_grad()

        # run encoder to get reconstructed point cloud
        last_out, out = encoder(G_batch)
        # print(out.shape)  # [N, 1, 3]

        # compute distance on masked points
        mask_offset = (out - x_original)[masked_nodes]  # [M, 3]
        mask_num = mask_offset.size(0)
        loss = mask_offset.norm(dim=-1, keepdim=False).mean(dim=0, keepdim=True)

        # compute global distance
        global_offset = (out - x_original)[~cls_nodes]
        global_dist = global_offset.norm(dim=-1, keepdim=False).mean(dim=0, keepdim=True)

        # get cls info
        cls_scalar = last_out['0'][cls_nodes].squeeze(-1).mean(dim=1).tolist()
        # print(last_out['0'][cls_nodes].shape)  # [batch_size, 8, 1]
        for scalar in cls_scalar:
            train_writer.add_scalar('cls/value', scalar)


        # Print
        train_writer.add_scalar('Loss/Global', global_dist.item(), it + iter_len * (epoch - 1))
        train_writer.add_scalar('Loss/Batch', loss.item(), it + iter_len * (epoch - 1))
        print(f'epoch {epoch} iter {it}/{iter_len} masks {mask_num} loss {loss.item()}')

        # backprop
        loss.backward()
        optimizer.step()

    torch.save({
        'epoch': epoch,
        'model_state_dict': encoder.state_dict(),
        'optimizer_state_dict': optimizer.state_dict(),
    }, f'checkpoints/epoch_{epoch}_encoder.pth')


def main():
    args = get_args()
    device = args.device
    torch.manual_seed(args.seed)

    train_dataset = ShapeNetDataset(args)
    train_loader = DataLoader(train_dataset, batch_size=args.batch_size, shuffle=True)
    torch.autograd.set_detect_anomaly(True)

    encoder = PointEncoder_SE3(
        num_layers=args.enc_nlayers,
        hidden=args.enc_hidden,
        num_degrees=args.max_deg + 1,
        attn_heads=args.enc_n_heads
    ).to(device)


    optimizer = optim.Adam(encoder.parameters(), lr=args.lr)

    if train_writer is not None:
        def printgradnorm_encoder(module, grad_input, grad_output):
            train_writer.add_scalar(f'Backward/Encoder/{module.__class__.__name__}', grad_input[0].norm().item())
        for layer in encoder.Gblock:
            layer.register_backward_hook(printgradnorm_encoder)
        encoder.GConv.register_backward_hook(printgradnorm_encoder)

    start_epoch = 12

    resume = True
    if resume:
        model_path = f'/home/staru/PycharmProjects/PCGAN/checkpoints/epoch_{start_epoch}_encoder.pth'
        encoder, optimizer, start_epoch = resume_pretrain(model_path, encoder, optimizer)
    graph_maker = MakeGraph(
        args.n_nbhd,
        device=device,
        use_cls=True,
        mask_rate=args.mask_rate,
        random_rate=args.random_rate)

    for epoch in range(start_epoch+1, 100):
        pretrain_epoch(epoch, graph_maker, encoder, train_loader, optimizer, args)
