import torch
from utils.nsample import RandomPointSampling
from config import cfg
import matplotlib.pyplot as plt


def main():
    device = cfg.device
    iter = 14300
    inf_location = f'/home/staru/PycharmProjects/PCGAN/checkpoints/iter_{iter}_Q.pth'
    gen_location = f'/home/staru/PycharmProjects/PCGAN/checkpoints/iter_{iter}_G.pth'

    inf_model = torch.load(inf_location)
    gen_model = torch.load(gen_location)
    inf_model.eval()
    gen_model.eval()


    obj_dir = '/DATA2/ShapeNet/ShapeNetCore.v2/02691156/10155655850468db78d106ce0a280f87/models/model_normalized.obj'
    inputsample = RandomPointSampling(128)
    with open(obj_dir, 'r') as obj:
        data = obj.read()

        lines = data.splitlines()
        vertices = []

        for line in lines:
            elememt = line.split()
            if elememt:
                if elememt[0] == 'v':
                    v = [float(elememt[1]), float(elememt[2]), float(elememt[3])]
                    vertices.append(v)
        with torch.no_grad():
            ptcloud_raw = torch.tensor(vertices)
            ptcloud = inputsample(ptcloud_raw.unsqueeze(0))

            ptcloud_avg = ptcloud.mean(dim=1, keepdim=True)  # [1, 1, 3]
            ptcloud_std = ptcloud.std(dim=1, keepdim=True)  # [1, 1, 3]
            ptcloud = (ptcloud - ptcloud_avg) / (ptcloud_std.norm(dim=-1, keepdim=True) + 1e-12)
            ptcloud = ptcloud.to(device)

            psi = inf_model(ptcloud)
            print(psi)
            new_ptcloud = gen_model(cfg.train.n_generate, psi)
            print(new_ptcloud)

            fig = plt.figure()

            #xs = ptcloud_raw[:,0].cpu().numpy()
            #ys = ptcloud_raw[:,1].cpu().numpy()
            #zs = ptcloud_raw[:,2].cpu().numpy()
            #ax = fig.add_subplot(111, projection='3d')
            #ax.scatter(xs, ys, zs)
            #xs = ptcloud[0,:,0].cpu().numpy()
            #ys = ptcloud[0,:,1].cpu().numpy()
            #zs = ptcloud[0,:,2].cpu().numpy()
            #ax = fig.add_subplot(111, projection='3d')
            #ax.scatter(xs, ys, zs)
            xs = new_ptcloud[0, :,0].cpu().numpy()
            ys = new_ptcloud[0, :,1].cpu().numpy()
            zs = new_ptcloud[0, :,2].cpu().numpy()
            ax = fig.add_subplot(111, projection='3d')
            ax.scatter(xs, ys, zs)
            plt.show()



if __name__ == "__main__":
    main()